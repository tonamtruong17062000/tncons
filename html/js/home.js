jQuery(document).ready(function($) {
$('#slider-linh-vuc').owlCarousel({
    center: true,
    items: 1,
    loop: true,
    margin: 0,
    nav:false,
    autoplay:false,
    dots:false,
    smartSpeed:1000,               
  });
  
$('#slider-doi-tac').owlCarousel({               
    items: 5,
    loop: true,
    margin: 10,
    nav:false,
    autoplay:true,
    dots:false,
    smartSpeed:1000,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1366:{
            items:5,
            nav:true,                        
        },
        1600:{
            items:5,
            nav:true,                       
        }
    },
  });
  $('#slider-tin-tuc').on('initialized.owl.carousel', function(e) {
    $("#slider-tin-tuc .owl-stage").css("margin-left","-100px");
    $("#slider-tin-tuc.owl-carousel .active").removeClass("center");
    $("#slider-tin-tuc.owl-carousel .active").first().addClass("center");
    
});
  $('#slider-tin-tuc').owlCarousel({               
    items: 3.5,
    loop: true,
    margin: 20,
    nav:false,
    autoplay:true,
    dots:false,
    smartSpeed:2000,
    center:false,
    animateOut: 'hideOut',
    animateIn: 'bounceInUp',
    
    responsive:{
        0:{
            items:1.3,
            nav:true
        },
        600:{
            items:1.5,
            nav:false
        },
        1366:{
            items:3,
            nav:true,                        
        },
        1600:{
            items:3.5,
            nav:true,                       
        }
    },
  });
  $('#slider-tin-tuc').on('dragged.owl.carousel', function(e) {
     
    $("#slider-tin-tuc .owl-stage").css("margin-left","-100px");
    $("#slider-tin-tuc.owl-carousel .active").removeClass("center");
    $("#slider-tin-tuc.owl-carousel .active").first().addClass("center");
    
});
$('#slider-tin-tuc').on('changed.owl.carousel', function(e) {
    setTimeout(function (){
  $("#slider-tin-tuc .owl-stage").css("margin-left","-100px");
  $("#slider-tin-tuc.owl-carousel .active").removeClass("center");
  $("#slider-tin-tuc.owl-carousel .active").first().addClass("center");
    },300)
});
});